#Testing cicd C
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "2.80.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg-project-1" {
  name     = "rg-project-1"
  location = var.location
}

resource "azurerm_virtual_network" "vn-project-1" {
  name                = "example-virtual-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.rg-project-1.location
  resource_group_name = azurerm_resource_group.rg-project-1.name
}
