variable "location" {
    type = string
    description = "Variable to set the location for virtual network"
}